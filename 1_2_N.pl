#! /usr/bin/perl

use warnings;
use strict;

# Copyright 2016 Ansgar Gruber
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

#   http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


print(
"\nPlease type/paste the string from which the 'n' and 'N' shall be counted).\n"
    );
my $input = <stdin>;
chomp($input);

(my $a, my $b )=ncounter($input);
my @pos=@{$a}; #positions of matches
my @len=@{$b}; #lengths of matches

# final print statement
print(
"\n\n".$input."\n\ncontains 'n' or 'N' occurences of the following lengths: \n\n".join(" ",@len)."\n\nat the following positions:\n\n".join(" ",@pos)."\n\n\n\nThank you for using 1_2_N.pl, please contact ansgar.gruber\@uni-konstanz.de with any question concerning the program :-\)\n"
);

# Subroutines

sub ncounter
{ # Matches blocks of "n" or "N" in a string, returns references to arrays with positions and lengths
    unless ( scalar(@_) == 1 ) {
        die(
"Subroutine ncounter needs exactly one element of \@\_ (in wich blocks of 'n' or 'N' will be matched), program died, please check.\n"
        );
    }
    else {
        my $seq=shift(@_);
        my @pos=(); #positions of matches
        my @len=(); #lengths of matches
        while ($seq =~ /(N+)/gi){
                push (@pos,(pos($seq)-length($1))); #positions of matches
                push (@len,length($1)); #lengths of matches
        }
        return ( \@pos, \@len );
    }
}    # end of sub ncounter